<?php require_once('Connections/conexion_admin_proyectos.php'); ?>  
<?php include('sis_acceso_ok.php'); ?>
<?php
mysql_select_db($database_conexion_proyectos, $conexion_admin_proyectos);
    $idtarea=$_GET['idtarea'];
    $consulta =mysql_query("SELECT * FROM tarea WHERE $idtarea=idtarea") or die(mysql_error());
    $row_consulta = mysql_fetch_array($consulta);

?>
<!DOCTYPE html>
<html lang="en">

<head>
<?php include "sis_header.php" ?>
</head>

<body style="background-color: white">

    <div id="wrapper">
        <!-- Navigation -->
        
        <?php include "sys_menu_vertical.php" ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Ver tarea
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-table"></i><a href="proyectos.php"> Proyectos</a>
                            </li>
                            <li>
                                <a href="tareas_listar.php"> Lista de tareas</a>
                            </li>
                            <li class="active">
                                 Ver
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- ---------------------------------------------Formulario------------------------------------------------------- -->
                <div id="resultado" class="row">  
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label>Numero de Tarea:</label>
                        <input type="text" class="form-control" disabled value="<?php echo $row_consulta['idtarea'] ?>" name="idtarea" id="idtarea">
                    </div>
                    <div class="form-group">
                        <label>Nombre:</label>
                        <input type="text" class="form-control" disabled value="<?php echo $row_consulta['nombre_tarea'] ?>" name="nombre">
                    </div>
                    <div class="form-group">
                        <label>Descripci&oacute;n:</label>
                        <input type="text" class="form-control" disabled value="<?php echo $row_consulta['descripcion_tarea'] ?>" name="descripcion">
                    </div>

                    <?php switch ($row_consulta['estado_tarea']) {
                        case 0:
                            $estadotarea = "Pendiente";
                            break;
                        case 1:
                            $estadotarea = "En curso";
                            break;
                        default:
                            $estadotarea = "Terminada";
                            break;
                    }?>
                     <?php switch ($row_consulta['prioridad']) {
                        case 1:
                            $prioridad = "Alta";
                            break;
                        case 2:
                            $prioridad = "Media";
                            break;
                        default:
                            $prioridad = "Baja";
                            break;
                    }?>
                     <div class="form-group">
                        <label>Prioridad:</label>
                        <input type="text" class="form-control" disabled value="<?php echo $prioridad ?>" >
                    </div>
                    <div class="form-group">
                        <label>Estado:</label>
                        <input type="text" class="form-control" disabled value="<?php echo $estadotarea ?>" >
                    </div> 
                    <p class="help-block">
                                <a href="#cambio_estado">
                                    <div id="mostrar_cambio"><i class="fa fa-unlock-alt" aria-hidden="true"></i>
                                   Modificar Estado
                                   </div>
                                </a>
                    </p>

                    <div class="form-group" id=cambio_estado style="display:none;">
                        <label>Nuevo Estado</label>
                             <select id="estadonuevo" class="form-control">
                                <option value="0">Pendiente</option>
                                <option selected value="1">En Curso</option>
                                <option value="2">Terminada</option>
                            </select>
                
                    </div> 

                    <div class="form-group">
                        <label>Fecha de incio (*):</label>
                        <input type="date" class="form-control" disabled value="<?php echo $row_consulta['fecha_inicio'] ?>">
                    </div>  
            
                    <div class="form-group">
                        <label>Fecha de finalizacion (*):</label>
                        <input type="date" class="form-control" disabled value="<?php echo $row_consulta['fecha_fin'] ?>">
                    </div>
                    <div class="form-group">
                        <label>Cantidad de personas:</label>
                        <input type="text" class="form-control" disabled value="<?php echo $row_consulta['cantidad_personas'] ?>">
                    </div>
                    <div class="form-group">
                        <label>Costo:</label>
                        <input type="text" class="form-control" disabled value="<?php echo $row_consulta['costo_tarea'] ?>">
                    </div>
                    
                    <div id="success"></div>
                    <button id="habilitar" class="btn btn-default pull-right">Aceptar</button>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <img src="images/icon.png">
                </div>
                </div>
          <!-- ---------------------------------------------------fin-------------------------------------------------------------- -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

      <script type="text/javascript">
        $(document).ready(function() {
        $('#mostrar_cambio').on('click', function(event) {
            $('#cambio_estado').toggle('slow');
        });
    });
    </script>

    <script type="text/javascript">
    $(document).ready(function() {
            $('#habilitar').click(function(event) {
                var estadonuevo = $('#estadonuevo').val();
                var idtarea= $('#idtarea').val();

                $.ajax({
                    type: "GET",
                    url: "tarea_modificacion_estado.php",
                    data: {"estadonuevo": estadonuevo, "idtarea": idtarea},
                    dataType: "html",
                    beforeSend: function(){
                          //imagen de carga
                          $("#resultado").html("<p align='center'><img src='images/ajax-loader.gif' /></p>");
                    },
                    error: function(){
                          alert("error petición ajax");
                    },
                    success: function(data){                                                    
                          $("#resultado").empty();
                          $("#resultado").append(data);                                     
                    }
              });
            });
        });
    </script>
    
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#principal').removeAttr('class');
            $('#proyecto').attr('class', 'active');    
        });
    </script>
    <!--
    <script type="text/javascript">
    $('#reset').click(function(event) {
        $('#nombre').val('');
        $('#descripcion').val('');
        $('#fechainicio').val('');
        $('#fechafin').val('');
    });
    </script>
-->
</body>

</html>
<?php require_once('Connections/conexion_admin_proyectos.php'); ?>
<?php include('sis_acceso_ok.php'); ?>
<?php 
    mysql_select_db($database_conexion_proyectos, $conexion_admin_proyectos);
    $q_persona=mysql_query("SELECT idpersona,nombre FROM persona WHERE tipo_persona_idtipo_persona=2");
?>
<!DOCTYPE html>
<html lang="en">

<head>
<?php include "sis_header.php" ?>
</head>

<body style="background-color: white">

    <div id="wrapper">
        <!-- Navigation -->
        
        <?php include "sys_menu_vertical.php" ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Nuevo proyecto
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-table"></i><a href="proyectos.php"> Proyectos</a>
                            </li>
                            <li>
                                    Agregar proyecto
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- ---------------------------------------------Formulario------------------------------------------------------- -->
                <div id="resultado" class="row">  
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label>Nombre (*):</label>
                        <input type="text" class="form-control" placeholder="Ingrese nombre del proyecto" id="nombre">
                    </div> 
                    <div class="form-group">
                        <label>Descripcion:</label>
                        <textarea class="form-control" rows="5" id="descripcion"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Fecha de incio (*):</label>
                        <input type="date" class="form-control" id="fechainicio">
                    </div>  
            
                    <div class="form-group">
                        <label>Fecha de finalizacion (*):</label>
                        <input type="date" class="form-control" id="fechafin">
                    </div>
                    
                    <div id="selectresponsable" class="form-group" ">
                        <label>Responsable:</label>
                        <select id="idpersona" class="form-control">
                        <option>Seleccione un Responsable</option>
                        <?php 
                        while ($row_persona=mysql_fetch_array($q_persona)) { 
                            ?>
                             <option value="<?php echo $row_persona['idpersona'] ?>"><?php echo $row_persona['nombre'] ?></option>
                         <?php } ?>
                        </select>
                    </div>
                    <div id="success"></div>
                    <button id="reset" class="btn btn-default">Limpiar</button>
                    <button id="habilitar" type="sumbit" class="btn btn-default pull-right" >Aceptar</button>
                </div>
                
                </div>
          <!-- ---------------------------------------------------fin-------------------------------------------------------------- -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <!-- JQUERY PARA COMPROBAR QUE NOMBRE, FECHA INICIO Y FECHA FIN SE INGRESEN -->

    <script type="text/javascript">
    $(document).ready(function() {

        $('#fechainicio').change(function(event) {
            comprobar();
         });

        $('#fechafin').change(function(event) {
            comprobar();
        });
       
        $('#nombre').change(function(event) {
            comprobar();
        });

        function comprobar(){

            var habilitai=$('#fechainicio').val();
            var habilitaf=$('#fechafin').val();
            var habilitan=$('#nombre').val();
            
            if (habilitai!='' && habilitan!='' && habilitaf!='') {
            $('#habilitar').removeAttr('disabled');
            }
           else{
            $('#habilitar').attr('disabled', true);;
            };
        }
    });
    </script>


    <script type="text/javascript">
    $(document).ready(function() {
            $('#habilitar').click(function(event) {
                var nombre = $('#nombre').val();
                var descripcion = $('#descripcion').val();
                var fechainicio = $('#fechainicio').val();
                var fechafin = $('#fechafin').val();
                var idpersona =$('#idpersona').val();

                $.ajax({
                    type: "POST",
                    url: "proyectos_alta_ok.php",
                    data: { "nombre": nombre, "descripcion": descripcion,"fechainicio":fechainicio, "fechafin": fechafin,"idpersona" : idpersona},
                    dataType: "html",
                    beforeSend: function(){
                          //imagen de carga
                          $("#resultado").html("<p align='center'><img src='images/ajax-loader.gif' /></p>");
                    },
                    error: function(){
                          alert("error petición ajax");
                    },
                    success: function(data){                                                    
                          $("#resultado").empty();
                          $("#resultado").append(data);                                     
                    }
              });
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#principal').removeAttr('class');
            $('#proyecto').attr('class', 'active');    
        });
    </script>
    <script type="text/javascript">
    $('#reset').click(function(event) {
        $('#nombre').val('');
        $('#descripcion').val('');
        $('#fechainicio').val('');
        $('#fechafin').val('');
    });
    </script>

</body>

</html>
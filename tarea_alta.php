<?php require_once('Connections/conexion_admin_proyectos.php'); ?>
<?php include('sis_acceso_ok.php'); ?>
<?php 
    mysql_select_db($database_conexion_proyectos, $conexion_admin_proyectos);
    $q_persona=mysql_query("SELECT idpersona,nombre FROM persona WHERE tipo_persona_idtipo_persona=1");
    $q_recurso=mysql_query("SELECT idrecurso_material,descripcion_recurso FROM recurso_material WHERE habilitacion=1");

    $idproyecto = $_GET['idproyecto'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
<?php include "sis_header.php" ?>
</head>

<body style="background-color: white">

    <div id="wrapper">
        <!-- Navigation -->
        
        <?php include "sys_menu_vertical.php" ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Nueva Tarea
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-table"></i><a href="proyectos.php"> Proyectos</a>
                            </li>
                            <li>
                                <a href="seleccionar_proyecto.php"> Seleccionar proyecto</a>
                            </li>
                            <li class="active">
                                 Agregar tareas
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- ---------------------------------------------Formulario------------------------------------------------------- -->
                <div id="resultado" class="row">  
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <input type="hidden" name="idproyecto" id="idproyecto" class="form-control" value="<?php echo $idproyecto; ?>">

                     <div class="form-group">
                        <label>Nombre(*):</label>
                        <input type="text" class="form-control" placeholder="Ingrese nombre de la tarea" id="nombretarea" required>
                    </div>
                    <div class="form-group">
                        <label>Descripci&oacute;n (*):</label>
                        <input type="text" class="form-control" placeholder="Ingrese descripci&oacute;n" id="descripcion" required>
                    </div>
                    <div class="form-group">
                        <label>Prioridad:</label>
                        <select id="prioridad" class="form-control" required="required">
                            <option value="3">Baja</option>
                            <option value="2">Media</option>
                            <option value="1">Alta</option>
                        </select>
                    </div> 
                    
                    <div class="form-group">
                        <label>Fecha de incio (*):</label>
                        <input type="date" class="form-control" id="fechainicio" required>
                    </div>  
            
                    <div class="form-group">
                        <label>Fecha de finalizacion (*):</label>
                        <input type="date" class="form-control" id="fechafin" required>
                    </div>

                    <div class="form-group">
                        <label>Costo:</label>
                        <input type="text" id="costotarea" class="form-control" value="" pattern="" title="">
                    </div>

<!--
LOS SELECT NO SE TRABAJANCOMO LOS INPUT POR LO QUE EN NAME NO TIENEN QUE IR LOS CORCHETES idpersona[](ESTA MAL)
DIRECTAMENTE LO TOMAS CON EL ID ESA CORRECCION SE HIZO EN LOS DOS SELECT
 -->

                    <div id="selectparticipante" class="form-group">
                        <label>Participante:</label>
                        <select name="idpersona" id="idpersona" multiple class='form-control'>
                        <?php 
                        while ($row_persona=mysql_fetch_array($q_persona)) { 
                            ?>
                             <option value="<?php echo $row_persona['idpersona'] ?>"><?php echo $row_persona['nombre'] ?></option>
                         <?php } ?>
                        </select>
                    </div>

                    <div id="selectrecursos" class="form-group">
                        <label>Asignar Recursos:</label>
                        <select name="idrecurso_material" id="idrecurso_material" multiple class='form-control'>
                        <?php 
                        while ($row_recurso=mysql_fetch_array($q_recurso)) { 
                            ?>
                             <option value="<?php echo $row_recurso['idrecurso_material'] ?>"><?php echo $row_recurso['descripcion_recurso'] ?></option>
                         <?php } ?>
                        </select>
                    </div>
                    
                    <div id="success"></div>
                    <button id="reset" class="btn btn-default">Limpiar</button>
                    <button id="habilitar" type='submit' class="btn btn-default pull-right">Aceptar</button>
                </div>
                </div>
          <!-- ---------------------------------------------------fin-------------------------------------------------------------- -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>



    <script type="text/javascript">
        $(document).ready(function() {
            $('#principal').removeAttr('class');
            $('#proyecto').attr('class', 'active');    
        });
    </script>
    
    <script type="text/javascript">
    $(document).ready(function() {
            $('#habilitar').click(function(event) {
                var nombretarea = $('#nombretarea').val();
                var descripcion = $('#descripcion').val();
                var prioridad = $('#prioridad').val();
                var fechainicio = $('#fechainicio').val();
                var fechafin = $('#fechafin').val();
                var costotarea= $('#costotarea').val();
                var idproyecto= $('#idproyecto').val();

/* 
EN ESTA PARTE TOMAS LOS VALORES DEL SELECT SIMPLEMENTE CON EL ID Y AL SER MULTIPLE TE TOMA TODOS LOS QUE ESTAN SELECIONADOS
ANTERIORMENTE TOMABAS EL VALOR DEL SELECT SIN HACER REFERENCIA A NINGUN NAME NI ID POR LO TANTO EN LAS DOS VARIABLES (selectedpersona y selectedrecurso) TOMABA LOS MISMOS DATOS Y AHI ESTABA EL ERROR EN LA CANTIDAD.
DE TODAS FORMAS SI HUBIESES REFERENCIADO BIEN LO MISMO IBAS A TENER UN ERROR YA QUE PARA TOMAR EL VALOR USABAS LA PROPIEDAD CHECKED QUE ES DE LOS CHECKBOX, EN LOS SELECTS SE USA
LA PROPIEDAD SELECTED.
LOS ERRORES EN EL OTRO ARCHIVO ERAN PROPIOS DE LO QUE ESTABAS MANDANDO DESDE ACA (ERROR DE CLAVE FORANEA) POR QUE INTENTABA GUARDAR UNA CLAVE FORANEA DE PERSONA EN LA TABLA DE RECURSOS_HAS_TAREA Y AL NO EXISTIR TE DABA ERROR.
Y POR ULTIMO ESTABAS MANDANDO UNA CADENA DESDE ACA Y EN EL OTRO ARCHIVO TENIAS Q USAR EL EXPLODE CUANDO NO ES NECESARIO. SI NECESITAS MANDAR UN ARRAY NO LO CONCATENES COMO UNA CADENA. DECLARALO COMO new Array() Y PARA AGREGARLE ELEMENTOS SE HACE PUSH EN EL ARRAY.

 */

                var selectedpersona= $('#idpersona').val();
                var selectedrecurso= $('#idrecurso_material').val();
                $.ajax({
                    url: "tarea_alta_ok.php",
                    type: "POST",
                    dataType: 'html',
                    data: {nombretarea: nombretarea, descripcion: descripcion,prioridad: prioridad, fechainicio: fechainicio,fechafin:fechafin,costotarea: costotarea,idproyecto: idproyecto, selectedpersona: selectedpersona, selectedrecurso:selectedrecurso},
                    beforeSend: function(){
                          //imagen de carga
                          $("#resultado").html("<p align='center'><img src='images/ajax-loader.gif' /></p>");
                    },
                    error: function(){
                          alert("error petición ajax");
                    },
                    success: function(data){                                                    
                          $("#resultado").empty();
                          $("#resultado").append(data);                                     
                    }
              });
            });
        });
    </script>

</body>

</html>
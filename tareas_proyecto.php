<?php require_once('Connections/conexion_admin_proyectos.php'); ?>
<?php include('sis_acceso_ok.php'); ?>
<?php

mysql_select_db($database_conexion_proyectos, $conexion_admin_proyectos);
   
$consulta=mysql_query("SELECT * FROM tarea WHERE proyecto_idproyecto=$idproyecto ORDER BY fecha_fin ASC");
$contar = mysql_num_rows($consulta);

/*SI NO SE ENCUENTRA NINGUNA TAREA */
    if($contar == 0){?>
    	<div class="row">
          <div class="col-lg-12">
              <div class="alert alert-danger alert-dismissable" align="center">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <i class="fa fa-times fa-2x" aria-hidden="true"></i>
                <br>
                No se encontraron tareas para el proyecto id: <?php echo $idproyecto; ?>.
              </div>
          </div>
      </div>
   <?php }
/* IMPRIME EN PANTALLA LAS TAREAS ENCONTREADAS */
    else{	?>
    <table class="table table-borderer">
        <tr>
    	  	<th class='col-xs-1 col-sm-1 col-md-1 col-lg-1'>Id</th>
					<th class='col-xs-3 col-sm-3 col-md-3 col-lg-3'>Descripcion de tarea</th>
          <th class='col-xs-2 col-sm-2 col-md-2 col-lg-2'>Prioridad</th>
					<th class='col-xs-2 col-sm-2 col-md-2 col-lg-2'>F. inicio</th>
					<th class='col-xs-2 col-sm-2 col-md-2 col-lg-2'>F. fin</th>
					<th class='col-xs-1 col-sm-1 col-md-1 col-lg-1'>Estado</th>
				</tr>
  <?php
        while($row_consulta=mysql_fetch_array($consulta)){

            $idtarea = $row_consulta['idtarea']; 
            $descripciontarea= $row_consulta['descripcion_tarea'];
            switch ($row_consulta['prioridad']) {
              case '1':
                $prioridad = "Alta";
                break;
              
              case '2':
                $prioridad = "Media";
                break;

              default:
                $prioridad = "Baja";
                break;
            }
            switch ($row_consulta['estado_tarea']) {
                    case '0':
                    $estado='Pendiente';
                    break;
                    case '1':
                    $estado = 'En curso';
                    break;
                    case '2':
                    $estado = 'Terminado';
                    break;
                    default:
                    $estado = 'No definido';
                    break;
                }
          $fechainicio=date("d/m/Y",strtotime($row_consulta['fecha_inicio']));
          $fechafin=date("d/m/Y",strtotime($row_consulta['fecha_fin']));
          $costotarea=$row_consulta['costo_tarea'];

          if ($row_consulta['estado_tarea'] != 2) {          
                     //------------------------------------------------------------------------------
                        $valoresPrimera = explode ("/", date("d/m/Y",strtotime("now")));   
                         $valoresSegunda = explode ("/", $fechafin); 
                         $diaPrimera    = $valoresPrimera[0];  
                         $mesPrimera  = $valoresPrimera[1];  
                         $anyoPrimera   = $valoresPrimera[2]; 
                         $diaSegunda   = $valoresSegunda[0];  
                         $mesSegunda = $valoresSegunda[1];  
                         $anyoSegunda  = $valoresSegunda[2];
                         $diasPrimeraJuliano = gregoriantojd($mesPrimera, $diaPrimera, $anyoPrimera);  
                         $diasSegundaJuliano = gregoriantojd($mesSegunda, $diaSegunda, $anyoSegunda);     
                         if(!checkdate($mesPrimera, $diaPrimera, $anyoPrimera)){
                           // "La fecha ".$primera." no es válida";
                           echo "fecha incio mal";
                         }else{
                           if(!checkdate($mesSegunda, $diaSegunda, $anyoSegunda)){
                           // "La fecha ".$segunda." no es válida";
                           echo "fecha fin mal";
                           }else{
                           $diasavencer =  $diasSegundaJuliano - $diasPrimeraJuliano;
                           } 
                         }
           
                     //-----------------------------------------------------------------------
                     
                     if ($diasavencer <= 0) { ?>
                       <tr style='color: #a94442; background-color: #f2dede; border-color: #ebccd1;'>
                         <td><a href='#'><?php echo $idtarea ?></a></td>
                         <td><?php echo $descripciontarea ?></td> 
                         <td><?php echo $prioridad ?></td>
                         <td><?php echo $fechainicio ?></td>
                         <td><?php echo $fechafin ?></td>
                         <td><?php echo $estado ?></td>
                       </tr>
                     <?php }
                     else{
                       if ($diasavencer < 5 && $diasavencer>0) { ?>
                         <tr style='color: #D35400; background-color: #FDEBD0; border-color: #f0ad4e; '>
                           <td><a href='#'><?php echo $idtarea ?></a></td>
                           <td><?php echo $descripciontarea ?></td> 
                           <td><?php echo $prioridad ?></td>
                           <td><?php echo $fechainicio ?></td>
                           <td><?php echo $fechafin ?></td>
                           <td><?php echo $estado ?></td>
                         </tr>
                       <?php }
                       else{ ?>
                         <tr>
                           <td><a href='#'><?php echo $idtarea ?></a></td>
                           <td><?php echo $descripciontarea ?></td> 
                           <td><?php echo $prioridad ?></td>
                           <td><?php echo $fechainicio ?></td>
                           <td><?php echo $fechafin ?></td>
                           <td><?php echo $estado ?></td>
                         </tr>
                      <?php }
                     }
          }
          

          else{ ?>

            <tr style='color: #3c763d; background-color: #dff0d8; border-color: #d6e9c6;'>
              <td><a href='#'><?php echo $idtarea ?></a></td>
              <td><?php echo $descripciontarea ?></td> 
              <td><?php echo $prioridad ?></td>
              <td><?php echo $fechainicio ?></td>
              <td><?php echo $fechafin ?></td>
              <td><?php echo $estado ?></td>
            </tr>
          <?php } 
        }
   }      

?>
</table>
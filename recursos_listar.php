<?php require_once('Connections/conexion_admin_proyectos.php'); ?>
<?php include('sis_acceso_ok.php'); ?>
<!DOCTYPE html>
<html lang="en">

<head>
<?php include "sis_header.php" ?>
</head>

<body style="background-color: white">
    <div id="wrapper">
        <!-- Navigation -->
        
        <?php include "sys_menu_vertical.php" ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Recursos
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-table"></i><a href="recurso.php"> Recursos</a>
                            </li>
                            <li class="active">
                                    Listado
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- ---------------------------------------------Buscador-------------------------------------------------- -->
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">   
                            <input class="form-control" placeholder="Nombre o id del recurso" id="appendedInputButton" type="text" name="busca_recurso">
                        </div>  
                        <br>
                        <br>
                        <br>
                        <div class="">
                            <table class="table table-borderer" id="resultado"></table>
                        </div>
                        <div class="clearfix"></div>

                
                <!-- ---------------------------------------------------fin------------------------------------------------ -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

 

</body>


<script type="text/javascript">
  $(document).ready(function() {
    //CARGA 100 MUESTRAS AL CARGAR LA PAGINA
   
    $.ajax({
            type: "GET",
            url: "recurso_select_inicial.php",
            dataType: "html",
            beforeSend: function(){
                  //imagen de carga
                  $("#resultado").html("<p align='center'><img src='images/ajax-loader.gif' /></p>");
            },
            error: function(){

                  alert("error petición ajax");
            },
            success: function(data){                                                    
                  
                  $("#resultado").empty();
                  $("#resultado").append(data);
                                                     
            }
         });

  //COMPROBAMOS SI SE PRESIONA ALGUNA TECLA

    $('#appendedInputButton').keyup(function(event) {
      //AL PRESIONAR UNA TECLA TOMAMOS EL VALOR DEL INPUT
      var busqueda = $('#appendedInputButton').val();
        //HACE LA BUSQUEDA Y DECUELVE UNA CADENA CON UNA TABLA                                                                            
              $.ajax({
                    type: "GET",
                    url: "recurso_select_key.php",
                    data: "b="+busqueda,
                    dataType: "html",
                    beforeSend: function(){
                          //imagen de carga
                          $("#resultado").html("<p align='center'><img src='images/ajax-loader.gif' /></p>");
                    },
                    error: function(){
                          alert("error petición ajax");
                    },
                    success: function(data){                                                    
                          $("#resultado").empty();
                          $("#resultado").append(data);
                                                             
                    }
              });             
    });
  });
</script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#principal').removeAttr('class');
            $('#recurso').attr('class', 'active');    
        });
    </script>

</html>
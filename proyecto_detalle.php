<?php require_once('Connections/conexion_admin_proyectos.php'); ?>
<?php include('sis_acceso_ok.php'); ?>
<?php
    
    mysql_select_db($database_conexion_proyectos, $conexion_admin_proyectos); 
    $idproyecto = $_GET['idproyecto'];
    //CAMBIA EL ESTADO AL PROYECTO EN CASO DE HACER CLICK EN EL ICONO DE PLAY EN LA LISTA DE PROYECTOS
    if (isset($_GET['iniciar'])) {
        mysql_query("UPDATE proyecto SET estado_proyecto='1' WHERE idproyecto=$idproyecto");
    }
    //------------------------------------------------------------------------------------------------

    //BUSCA EL PROYECTO SELECCIONADO EN LA BASE DE DATOS
    $consulta = mysql_query("SELECT * FROM proyecto WHERE idproyecto = $idproyecto AND estado_proyecto!=2") or die("Error: <br>".mysql_error());
    $row_consulta = mysql_fetch_array($consulta);
    $q_persona=mysql_query("SELECT idpersona,nombre FROM persona WHERE idpersona=$row_consulta[persona_idpersona]");
    $row_persona=mysql_fetch_array($q_persona);

    $tareas=mysql_query("SELECT estado_tarea, costo_tarea, cantidad_personas FROM tarea WHERE proyecto_idproyecto=$idproyecto");
          $total_tareas = mysql_num_rows($tareas);
          $tareas_completadas=0;
          $costo_total_proyecto=0;
          $total_personas=1;

          while ($row_tareas=mysql_fetch_array($tareas)) {
            $costo_total_proyecto=$costo_total_proyecto+$row_tareas['costo_tarea'];
            $total_personas=$total_personas+$row_tareas['cantidad_personas'];
            if ($row_tareas['estado_tarea']==2) {
              $tareas_completadas=$tareas_completadas+1;
            }
          }

          if ($total_tareas==0) {
            $porcentaje_tareas=0;
          }
          else{
            $porcentaje_tareas= round($tareas_completadas*100/$total_tareas, 2);
          }
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include "sis_header.php" ?>
</head>
<body style="background-color: white">
    <div id="wrapper">
        <!-- Navigation -->
        <?php include "sys_menu_vertical.php" ?>
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Proyectos <small>Detalle</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-table"></i><a href="proyectos.php"> Proyectos</a>
                            </li>
                            <li>
                                    <a href="ver_proyectos.php"> Listado</a>
                            </li>
                            <li class="active">
                                    Detalle
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- ---------------------------------------------Formulario----------------------------------------------- -->
                <div class="row">
                    <label>Avance: proyecto id <?php echo $row_consulta['idproyecto'] ?></label>
                    <legend></legend>
                    <div class="progress progress-striped active">
                        <div class="progress-bar progress-bar-success" role="progressbar"  aria-valuenow="<?php echo $porcentaje_tareas ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $porcentaje_tareas ?>%;">
                            <span style="color: black;"><?php echo $tareas_completadas." / ".$total_tareas ?> Tareas completadas</span>
                        </div>
                    </div>
                    <legend></legend>
                </div>

                <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label>Nombre:</label>
                        <input type="text" readonly class="form-control" value="<?php echo $row_consulta['nombre_proyecto'] ?>" name="nombre">
                    </div> 
                    <div class="form-group">
                        <label>Responsable del Proyecto</label>
                        <input type="text" readonly class="form-control" value=<?php echo $row_persona['nombre'] ?> name="idproyecto">
                    </div> 
                    <div class="form-group">
                        <label>Descripcion:</label>
                        <textarea class="form-control" readonly rows="3 " name="descripcion"><?php echo $row_consulta['descripcion_proyecto'] ?></textarea>
                    </div>
                </div>
                <?php 
                switch ($row_consulta['estado_proyecto']) {
                    case '0':
                    $estado='Pendiente';
                    break;
                    case '1':
                    $estado = 'En curso';
                    break;
                    case '2':
                    $estado = 'Terminado';
                    break;
                    default:
                    $estado = 'No definido';
                    break;
                }                
                ?>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label>Estado:</label>
                        <input type="text" readonly class="form-control" value="<?php echo $estado ?>" name="estado_proyecto">
                    </div> 
                    <div class="form-group">
                        <label>Fecha de incio:</label>
                        <input type="date" readonly class="form-control" value="<?php echo $row_consulta['fecha_inicio_proyecto'] ?>" name="fechainicio">
                    </div>  
                
                    <div class="form-group">
                        <label>Fecha de finalizacion:</label>
                        <input type="date" readonly class="form-control" value="<?php echo $row_consulta['fecha_fin_proyecto'] ?>" name="fechafin">
                    </div>                    
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label>Costo Total:</label>
                        <input type="text" readonly class="form-control" value="<?php echo '$  '.$costo_total_proyecto ?>" name="nombre">
                    </div>
                    <div class="form-group">
                        <label>Cantidad de personas:</label>
                        <input type="text" readonly class="form-control" value="<?php echo $total_personas ?>" name="nombre">
                    </div>
                    <div class="form-group">
                        <label>Referencias tabla de tareas</label>
                        <div class="progress">
                            <div class="progress-bar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 100%; background-color: #dff0d8;">
                                <span style="color: black;">Finalizadas</span>                                
                            </div>
                        </div>
                        <div class="progress">
                            <div class="progress-bar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 100%; background-color: #FDEBD0;;">
                                <span style="color: black;">Por vencer</span>
                            </div>
                        </div>
                        <div class="progress">
                            <div class="progress-bar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 100%; background-color: #f2dede;">
                                <span style="color: black;">Vencidas</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="success"></div>
                <legend></legend>
                <?php include "tareas_proyecto.php"; ?>
                </div>       
                
          <!-- ---------------------------------------------------fin------------------------------------------------------ -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#principal').removeAttr('class');
            $('#proyecto').attr('class', 'active');    
        });
    </script>

</body>

</html>